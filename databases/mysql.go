package databases

import (
	"database/sql"
	"needs/config"

	_ "github.com/go-sql-driver/mysql"
)

// DbConnSlave connection database use slave
func DbConnSlave() (db *sql.DB) {
	db, err := sql.Open("mysql", config.Sqlinfo)
	if err != nil {
		panic(err.Error())
	}
	return db
}

// DbConnMaster connection database use master
func DbConnMaster() (db *sql.DB) {
	db, err := sql.Open("mysql", config.MasterSqlinfo)
	if err != nil {
		panic(err.Error())
	}
	return db
}
