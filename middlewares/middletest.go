package middlewares

import (
	"needs/helpers"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func Middtest(ctx *gin.Context) {
	ctx.String(http.StatusOK, "im middlewares")
}

func CheckToken(c *gin.Context) {
	token := c.GetHeader("utoken")
	session := sessions.Default(c)
	vid := session.Get(token)
	if vid == nil {
		result := &helpers.Result{
			Code:    "0",
			Message: "token not found",
		}
		c.JSON(200, result)
		c.Abort()
	}
	c.Next()
}
