package brands

import (
	"needs/errors"
	"needs/helpers"
	"needs/models"
	"needs/models/store"

	"github.com/gin-gonic/gin"
)

// CreateBrand : 建立品牌
func CreateBrand(c *gin.Context) {
	token := c.GetHeader("utoken")
	uid := helpers.GetUIDByToken(token, c)
	name := c.PostForm("name")
	url := c.PostForm("url")
	coverImg := c.PostForm("cover_img")
	img := c.PostForm("img")
	brand := &models.Brand{
		OwnerID:  uid,
		Name:     name,
		URL:      url,
		CoverImg: coverImg,
		Img:      img,
		Status:   1,
	}
	result := &helpers.Result{}
	bid, err := store.BrandCreate(brand)
	if err != nil {
		result.Code = "0"
		result.Message = errors.FAILED
		c.JSON(200, result)
	}
	// 建立user與brand關聯
	err = store.CreateConnUserBrand(uid, bid)
	if err != nil {
		result.Code = "0"
		result.Message = errors.FAILED
		c.JSON(200, result)
	}

	result.Code = "1"
	result.Message = errors.SUCCESS
	c.JSON(200, result)
}

/*func GetList() {
	token := c.GetHeader("utoken")
	uid := helpers.GetUIDByToken(token, c)

}*/
