package users

import (
	"crypto/rand"
	"encoding/base64"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var (
	googleOauthConfig = &oauth2.Config{
		RedirectURL:  "http://localhost:8081/GoogleCallback",
		ClientID:     "572937517517-rckqvcv8bq0v50dl864hnj9prssn50qj.apps.googleusercontent.com",
		ClientSecret: "Of5VFGZRCBMEHjiK9Y1lyl3d",
		Scopes: []string{"https://www.googleapis.com/auth/userinfo.profile",
			"https://www.googleapis.com/auth/userinfo.email"},
		Endpoint: google.Endpoint,
	}
	// Some random string, random for each request
	oauthStateString = randToken()
)

func randToken() string {
	b := make([]byte, 32)
	rand.Read(b)
	return base64.StdEncoding.EncodeToString(b)
}

// GetGoogleLoginURL 取得登入的ＵＲＬ
func GetGoogleLoginURL(c *gin.Context) {
	url := googleOauthConfig.AuthCodeURL(oauthStateString)
	c.String(200, url)
}

// GoogleCallback 取得登入後的資訊
func GoogleCallback(c *gin.Context) {
	code := c.Query("code")

	tok, err := googleOauthConfig.Exchange(oauth2.NoContext, code)

	if err != nil {
		panic(err)
	}

	service := googleOauthConfig.Client(oauth2.NoContext, tok)

	resp, err := service.Get("https://www.googleapis.com/oauth2/v3/userinfo")
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	defer resp.Body.Close()
	data, _ := ioutil.ReadAll(resp.Body)
	log.Println("resp body:", string(data))
	c.String(200, code)

}
