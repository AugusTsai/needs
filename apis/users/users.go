package users

import (
	"database/sql"
	"fmt"
	"needs/errors"
	"needs/helpers"
	"needs/models"
	"needs/models/store"
	"regexp"
	"time"

	"github.com/gin-gonic/gin"
)

// UpdateUser : form for update
type UpdateUser struct {
	Name    string `form:"name" json:"name" binding:"required"`
	UserURL string `form:"url" json:"url" binding:"required"`
	AboutME string `form:"about_me" json:"about_me"`
	Sex     string `form:"sex" json:"sex" binding:"required"`
}

// SignUp 註冊會員
func SignUp(c *gin.Context) {
	account := c.PostForm("account")
	password := helpers.GetMD5Hash(c.PostForm("password"))
	name := c.PostForm("name")

	result := &helpers.Result{}
	// validate email
	if m, _ := regexp.MatchString(`^([\w\.\_]{2,10})@(\w{1,}).([a-z]{2,4})$`, account); !m {
		result.Code = "0"
		result.Message = errors.EMAILERR
		c.JSON(200, result)
		return
	}

	// check email
	userDate, _ := store.GetUserInfoByAccound(account)

	if userDate != nil && userDate.ID > 0 {
		result.Code = "0"
		result.Message = errors.EMAILDUP
		c.JSON(200, result)
		return
	}

	user := &models.User{
		Account:  account,
		Password: password,
		Name:     name,
		Status:   1,
	}

	// signup by email
	userID, err := store.UserSignUP(user)
	if err != nil {
		result.Code = "0"
		result.Message = errors.FAILED
		c.JSON(200, result)
		return
	}

	Data := make(map[string]interface{})
	Data["account"] = user.Account
	Data["id"] = userID
	result.Data = Data
	result.Code = "1"
	result.Message = errors.SUCCESS
	c.JSON(200, result)

}

// Login 會員登入
func Login(c *gin.Context) {
	account := c.PostForm("account")
	password := helpers.GetMD5Hash(c.PostForm("password"))

	result := &helpers.Result{}
	// check email
	userDate, err := store.GetUserInfoByAccound(account)
	if err == sql.ErrNoRows {
		result.Code = "0"
		result.Message = errors.EMAILNOF
		c.JSON(200, result)
		return
	} else if err != nil {
		helpers.CheckErr(err)
	}

	// check password
	if password != userDate.Password {
		result.Code = "0"
		result.Message = errors.PWDERR
		c.JSON(200, result)
		return
	}

	// check user session
	session, err := store.GetSession(userDate.ID)
	if err == sql.ErrNoRows {
		session = fmt.Sprintf("%d_%s", userDate.ID, randToken())
		// store session
		err = store.LoginSession(userDate.ID, session)
		helpers.CheckErr(err)
	} else if err != nil {
		helpers.CheckErr(err)
	}

	Data := make(map[string]interface{})
	Data["account"] = account
	Data["session"] = session

	result.Code = "1"
	result.Data = Data
	result.Message = errors.SUCCESS
	c.JSON(200, result)
}

// Logout 會員登出
func Logout(c *gin.Context) {
	result := &helpers.Result{}
	token := c.GetHeader("utoken")
	// get user_id
	uID, err := store.GetUserIDBySession(token)
	if err == sql.ErrNoRows {
		result.Code = "0"
		result.Message = errors.TOKENNUL
		c.JSON(200, result)
	} else if err == nil {
		// delete session
		err = store.DeleteSession(uID)
		helpers.CheckErr(err)
		result.Code = "1"
		result.Message = errors.SUCCESS
		c.JSON(200, result)
	} else {
		result.Code = "0"
		result.Message = errors.FAILED
		c.JSON(200, result)
	}
}

// GetUserInfo : get user infomation
func GetUserInfo(c *gin.Context) {
	result := &helpers.Result{}
	token := c.GetHeader("utoken")
	// get user_id
	uID, err := store.GetUserIDBySession(token)
	if err == sql.ErrNoRows {
		result.Code = "0"
		result.Message = errors.TOKENNUL
		c.JSON(200, result)
	} else if err == nil {
		userDate, err := store.GetUserInfoByID(uID)
		if err != nil {
			result.Code = "0"
			result.Message = errors.FAILED
			c.JSON(200, result)
		}
		Data := make(map[string]interface{})
		// hidden user password
		userDate.Password = ""
		Data["user_info"] = userDate
		result.Code = "1"
		result.Data = Data
		result.Message = errors.SUCCESS
		c.JSON(200, result)
	} else {
		result.Code = "0"
		result.Message = errors.FAILED
		c.JSON(200, result)
	}
}

// UpdateInfo : update user info not include pwd,email
func UpdateInfo(c *gin.Context) {
	result := &helpers.Result{}
	token := c.GetHeader("utoken")
	// get user_id
	uID, err := store.GetUserIDBySession(token)
	if err == sql.ErrNoRows {
		result.Code = "0"
		result.Message = errors.TOKENNUL
		c.JSON(200, result)
		return
	} else if err != nil {
		result.Code = "0"
		result.Message = errors.FAILED
		c.JSON(200, result)
		return
	}
	var form UpdateUser
	if err := c.ShouldBind(&form); err == nil {
		userDate, err := store.GetUserInfoByID(uID)
		if err != nil {
			result.Code = "0"
			result.Message = errors.FAILED
			c.JSON(200, result)
			return
		}
		// if user change name 30 days/1 per
		if userDate.Name != form.Name {
			nameUpdateTime, err := store.GetNameUpdateTime(uID)
			if err != nil {
				result.Code = "0"
				result.Message = err.Error()
				c.JSON(200, result)
				return
			}
			if nameUpdateTime < 30 {
				result.Code = "0"
				result.Message = "can not update name"
				c.JSON(200, result)
				return
			}
			t := time.Now()
			updateNameTime := t.Format("2006-01-02 15:04:05")
			if err := store.UpdateNameTime(updateNameTime, uID); err != nil {
				result.Code = "0"
				result.Message = err.Error()
				c.JSON(200, result)
			}
		}

		if err := store.UpdateInfo(form.Name, form.UserURL, form.Sex, form.AboutME, uID); err == nil {
			result.Code = "1"
			result.Message = errors.SUCCESS
			c.JSON(200, result)
		} else {
			result.Code = "0"
			result.Message = err.Error()
			c.JSON(200, result)
		}
	} else {
		result.Code = "0"
		result.Message = err.Error()
		c.JSON(200, result)
	}
}

// UpdateEmail : update user account
func UpdateEmail(c *gin.Context) {
	account := c.PostForm("account")
	token := c.GetHeader("utoken")
	result := &helpers.Result{}
	// validate email
	if m, _ := regexp.MatchString(`^([\w\.\_]{2,10})@(\w{1,}).([a-z]{2,4})$`, account); !m {
		result.Code = "0"
		result.Message = errors.EMAILERR
		c.JSON(200, result)
		return
	}

	// get user_id
	uID, err := store.GetUserIDBySession(token)
	if err == sql.ErrNoRows {
		result.Code = "0"
		result.Message = errors.TOKENNUL
		c.JSON(200, result)
		return
	} else if err != nil {
		result.Code = "0"
		result.Message = errors.FAILED
		c.JSON(200, result)
		return
	}

	// check email
	userDate, _ := store.GetUserInfoByAccound(account)

	if userDate != nil && userDate.ID > 0 {
		result.Code = "0"
		result.Message = errors.EMAILDUP
		c.JSON(200, result)
		return
	}
	err = store.UpdateEmail(account, uID)

	if err != nil {
		result.Code = "0"
		result.Message = errors.FAILED
		c.JSON(200, result)
		return
	}

	result.Code = "1"
	result.Message = errors.SUCCESS
	c.JSON(200, result)
}

// UpdatePassWord : update user password
func UpdatePassWord(c *gin.Context) {
	password := helpers.GetMD5Hash(c.PostForm("password"))
	token := c.GetHeader("utoken")
	result := &helpers.Result{}

	// get user_id
	uID, err := store.GetUserIDBySession(token)
	if err == sql.ErrNoRows {
		result.Code = "0"
		result.Message = errors.TOKENNUL
		c.JSON(200, result)
		return
	} else if err != nil {
		result.Code = "0"
		result.Message = errors.FAILED
		c.JSON(200, result)
		return
	}

	err = store.UpdatePassWord(password, uID)

	if err != nil {
		result.Code = "0"
		result.Message = errors.FAILED
		c.JSON(200, result)
		return
	}

	result.Code = "1"
	result.Message = errors.SUCCESS
	c.JSON(200, result)
}
