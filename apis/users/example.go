package users

import (
	"needs/helpers"
	"needs/models/store"

	"github.com/gin-gonic/gin"
)

// ExampleTest : for test
func ExampleTest(c *gin.Context) {
	token := c.GetHeader("utoken")
	uid := helpers.GetUIDByToken(token, c)
	date, err := store.GetBrandListByUserID(uid)
	helpers.CheckErr(err)
	c.JSON(200, gin.H{
		"id": date,
	})

	/*c.JSON(200, gin.H{
		"uid": uid,
	})*/
	/*
		session := sessions.Default(c)
		vid := session.Get(5)
		if vid == nil {
			c.String(200, "session uid null")
		} else {
			c.JSON(200, gin.H{
				"id": vid,
			})
		}
		c.String(200, "test success")
	*/
}
