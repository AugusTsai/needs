package main

import (
	"needs/apis/brands"
	"needs/apis/users"
	"needs/middlewares"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func main() {
	app := gin.Default()

	store := sessions.NewCookieStore([]byte("secret"))
	app.Use(sessions.Sessions("needssession", store))

	// call host/api/users/example
	usersGrp := app.Group("/api/users")
	usersGrp.GET("/")
	usersGrp.GET("/example", middlewares.CheckToken, users.ExampleTest)
	usersGrp.POST("/login", users.Login)
	usersGrp.GET("/login/google", users.GetGoogleLoginURL)
	usersGrp.POST("/signup", users.SignUp)
	usersGrp.GET("/logout", users.Logout)
	usersGrp.POST("/update/info", users.UpdateInfo)
	usersGrp.GET("/info", users.GetUserInfo)
	usersGrp.POST("/update/email", users.UpdateEmail)
	usersGrp.POST("/update/pwd", users.UpdatePassWord)

	brandGrp := app.Group("/api/brands")
	brandGrp.GET("/")
	brandGrp.POST("/signup", brands.CreateBrand)

	app.GET("/hello", func(ctx *gin.Context) {
		ctx.String(http.StatusOK, "hello %s", "augus")
	})

	app.GET("/GoogleCallback", users.GoogleCallback)

	app.Run(":8081")
}
