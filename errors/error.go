package errors

const (
	SUCCESS    = "Success"
	FAILED     = "Failed"
	ACCOUNTDUP = "Duplicate account"
	PWDERR     = "wrong password"
	EMAILERR   = "wrong email"
	EMAILDUP   = "Email already exists"
	EMAILNOF   = "Email not found"
	TOKENNUL   = "session uid null"
	FORMFAILE  = "Field validation Post Form"
)
