package models

// User : 會員
type User struct {
	ID       int    `json:"id"`
	Account  string `json:"account"`
	Password string `json:"password"`
	Name     string `json:"name"`
	Picture  string `json:"img"`
	Birthday string `json:"birthday"`
	Phone    string `json:"phone"`
	Status   int    `json:"status"`
	Sex      int    `json:"sex"`
	AboutMe  string `json:"about_me"`
}
