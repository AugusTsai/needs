package models

// Brand : 品牌
type Brand struct {
	ID       int    `json:"id"`
	OwnerID  int    `json:"owner_id"`
	Name     string `json:"name"`
	URL      string `json:"url"`
	CoverImg string `json:"cover_img"`
	Img      string `json:"img"`
	Status   int    `json:"stauts"`
}
