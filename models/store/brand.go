package store

import (
	"database/sql"
	"needs/databases"
	"needs/helpers"
	"needs/models"
)

// BrandCreate : 品牌註冊
func BrandCreate(Brand *models.Brand) (BrandID int64, err error) {
	dbMaster := databases.DbConnMaster()
	defer dbMaster.Close()
	sqlContent := "INSERT INTO brand_list "
	sqlContent += "(`owner_id`,`name`,`url`,`cover_img`,`img`,`status`)"
	sqlContent += " VALUES (?,?,?,?,?,?)"
	stmt, err := dbMaster.Prepare(sqlContent)
	helpers.CheckErr(err)
	defer stmt.Close()

	res, err := stmt.Exec(
		Brand.OwnerID,
		Brand.Name,
		Brand.URL,
		Brand.CoverImg,
		Brand.Img,
		Brand.Status,
	)
	bid, err := res.LastInsertId()
	helpers.CheckErr(err)

	return bid, err
}

// CreateConnUserBrand : 建立會員跟品牌的關聯
func CreateConnUserBrand(UserID int, BrandID int64) (err error) {
	dbMaster := databases.DbConnMaster()
	defer dbMaster.Close()
	sqlContent := "INSERT INTO user_brand_list "
	sqlContent += "(`user_id`,`brand_id`)"
	sqlContent += " VALUES (?,?)"
	stmt, err := dbMaster.Prepare(sqlContent)
	helpers.CheckErr(err)
	defer stmt.Close()
	_, err = stmt.Exec(UserID, BrandID)
	helpers.CheckErr(err)

	return err
}

// GetBrandListByUserID : 取得品牌清單
func GetBrandListByUserID(UserID int) (BrandID map[int]interface{}, err error) {
	dbSlave := databases.DbConnSlave()
	defer dbSlave.Close()
	stmtOut, err := dbSlave.Prepare("SELECT brand_id FROM user_brand_list WHERE user_id = ?")
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	defer stmtOut.Close()
	rows, err := stmtOut.Query(UserID)
	helpers.CheckErr(err)
	defer rows.Close()
	var brandID int

	data := make(map[int]interface{})
	for i := 1; rows.Next(); i++ {
		rows.Scan(&brandID)
		brandInfo, err := GetBrandInfoByID(brandID)
		if err != nil {
			return nil, err
		}
		data[i] = brandInfo
	}

	return data, err
}

// GetBrandInfoByID : 取得品牌資訊透過品牌的id
func GetBrandInfoByID(BrandID int) (ret *models.Brand, err error) {
	dbSlave := databases.DbConnSlave()
	defer dbSlave.Close()
	data := models.Brand{}
	sqlContent := "SELECT"
	sqlContent += " `id`,`owner_id`,`name`,`url`,`cover_img`,`img`,`status` "
	sqlContent += "FROM `brand_list` WHERE `id` = ?"
	err = dbSlave.QueryRow(sqlContent, BrandID).Scan(
		&data.ID,
		&data.OwnerID,
		&data.Name,
		&data.URL,
		&data.CoverImg,
		&data.Img,
		&data.Status,
	)

	if err == nil {
		ret = &data
	} else if err == sql.ErrNoRows {
		return nil, err
	}
	return
}
