package store

import (
	"database/sql"
	"needs/databases"
	"needs/helpers"
	"needs/models"
)

// GetUserInfoByAccound : 取得會員的資訊透過帳號(email)
func GetUserInfoByAccound(Account string) (ret *models.User, err error) {
	dbSlave := databases.DbConnSlave()
	defer dbSlave.Close()
	data := models.User{}
	sqlContent := "SELECT"
	sqlContent += " `id`,`account`,`pwd`,`name`,`birthday`,`phone`,`status`,"
	sqlContent += " `sex`,`img`,`about_me` "
	sqlContent += "FROM `user` WHERE `account` = ?"
	err = dbSlave.QueryRow(sqlContent, Account).Scan(
		&data.ID,
		&data.Account,
		&data.Password,
		&data.Name,
		&data.Birthday,
		&data.Phone,
		&data.Status,
		&data.Sex,
		&data.Picture,
		&data.AboutMe,
	)

	if err == nil {
		ret = &data
	} else if err == sql.ErrNoRows {
		return nil, err
	}
	return
}

// UserSignUP : 會員註冊
func UserSignUP(User *models.User) (UserID int64, err error) {
	dbMaster := databases.DbConnMaster()
	defer dbMaster.Close()
	stmt, err := dbMaster.Prepare("INSERT INTO user (account,pwd,name,status) VALUES (?,?,?,?)")
	helpers.CheckErr(err)
	defer stmt.Close()

	res, err := stmt.Exec(User.Account, User.Password, User.Name, User.Status)
	helpers.CheckErr(err)

	uid, err := res.LastInsertId()
	helpers.CheckErr(err)

	return uid, err

}

// LoginSession : user login get session
func LoginSession(UserID int, Session string) (err error) {
	dbMaster := databases.DbConnMaster()
	defer dbMaster.Close()
	stmt, err := dbMaster.Prepare("INSERT INTO session (user_id,session) VALUES (?,?)")
	helpers.CheckErr(err)
	defer stmt.Close()
	_, err = stmt.Exec(UserID, Session)
	helpers.CheckErr(err)

	return err
}

// DeleteSession : user logout delete session
func DeleteSession(UserID int) (err error) {
	dbMaster := databases.DbConnMaster()
	defer dbMaster.Close()
	stmt, err := dbMaster.Prepare("DELETE FROM session WHERE user_id=?")
	helpers.CheckErr(err)
	defer stmt.Close()
	_, err = stmt.Exec(UserID)
	helpers.CheckErr(err)

	return err
}

// GetSession : get user's session
func GetSession(UserID int) (session string, err error) {
	dbSlave := databases.DbConnSlave()
	defer dbSlave.Close()
	var s string
	err = dbSlave.QueryRow("SELECT session FROM session WHERE user_id=?", UserID).Scan(&s)
	if err != nil {
		return "", err
	}

	return s, err
}

// GetUserIDBySession : get user_id by session
func GetUserIDBySession(Session string) (UserID int, err error) {
	dbSlave := databases.DbConnSlave()
	defer dbSlave.Close()
	var uID int
	err = dbSlave.QueryRow("SELECT user_id FROM session WHERE session=?", Session).Scan(&uID)
	if err != nil {
		return 0, err
	}

	return uID, err
}

// GetUserInfoByID : get user infomation by user_id
func GetUserInfoByID(UID int) (ret *models.User, err error) {
	dbSlave := databases.DbConnSlave()
	defer dbSlave.Close()
	data := models.User{}
	sqlContent := "SELECT"
	sqlContent += " `id`,`account`,`pwd`,`name`,`birthday`,`phone`,`status`,"
	sqlContent += " `sex`,`img`,`about_me` "
	sqlContent += "FROM `user` WHERE `id` = ?"
	err = dbSlave.QueryRow(sqlContent, UID).Scan(
		&data.ID,
		&data.Account,
		&data.Password,
		&data.Name,
		&data.Birthday,
		&data.Phone,
		&data.Status,
		&data.Sex,
		&data.Picture,
		&data.AboutMe,
	)

	if err == nil {
		ret = &data
	} else if err == sql.ErrNoRows {
		return nil, err
	}
	return
}

// UpdateInfo : update user info
func UpdateInfo(Name string, URL string, Sex string, AboutME string, UserID int) (err error) {
	dbMaster := databases.DbConnMaster()
	defer dbMaster.Close()
	stmt, err := dbMaster.Prepare("UPDATE user SET name=? , url=? , sex=? ,about_me=? WHERE id=?")
	helpers.CheckErr(err)
	defer stmt.Close()
	_, err = stmt.Exec(Name, URL, Sex, AboutME, UserID)
	helpers.CheckErr(err)

	return err
}

// GetNameUpdateTime : get how much days after user update name
func GetNameUpdateTime(UserID int) (t int, err error) {
	dbSlave := databases.DbConnSlave()
	defer dbSlave.Close()

	var nameUpdateTime int
	err = dbSlave.QueryRow("SELECT DATEDIFF(NOW(),name_update) FROM user WHERE id=?", UserID).Scan(&nameUpdateTime)
	if err != nil {
		return
	}

	return nameUpdateTime, err
}

// UpdateNameTime : when user update name update the cloumn name_update to now
func UpdateNameTime(UpdateTime string, UserID int) (err error) {
	dbMaster := databases.DbConnMaster()
	defer dbMaster.Close()
	stmt, err := dbMaster.Prepare("UPDATE user SET name_update=? WHERE id=?")
	helpers.CheckErr(err)
	defer stmt.Close()
	_, err = stmt.Exec(UpdateTime, UserID)
	helpers.CheckErr(err)

	return err
}

// UpdateEmail : update user's account (email)
func UpdateEmail(Email string, UserID int) (err error) {
	dbMaster := databases.DbConnMaster()
	defer dbMaster.Close()
	stmt, err := dbMaster.Prepare("UPDATE user SET account=? WHERE id=?")
	helpers.CheckErr(err)
	defer stmt.Close()
	_, err = stmt.Exec(Email, UserID)
	helpers.CheckErr(err)

	return err
}

// UpdatePassWord : update user's password
func UpdatePassWord(Pwd string, UserID int) (err error) {
	dbMaster := databases.DbConnMaster()
	defer dbMaster.Close()
	stmt, err := dbMaster.Prepare("UPDATE user SET pwd=? WHERE id=?")
	helpers.CheckErr(err)
	defer stmt.Close()
	_, err = stmt.Exec(Pwd, UserID)
	helpers.CheckErr(err)

	return err
}
