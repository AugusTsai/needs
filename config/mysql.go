package config

import "fmt"

// for slave
var host = "10.211.55.4"
var port = "3306"
var user = "augus"
var password = "1234"
var dbname = "needs"

// Sqlinfo for connectin db slave
var Sqlinfo = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", user, password, host, port, dbname)

// for master
var masterHost = "10.211.55.4"
var masterPort = "3306"
var masterUser = "augus"
var masterPassword = "1234"
var masterDbname = "needs"

// MasterSqlinfo for connection db master
var MasterSqlinfo = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", masterUser, masterPassword, masterHost, masterPort, masterDbname)
