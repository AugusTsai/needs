package helpers

import (
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"

	"github.com/gin-contrib/sessions"
)

type Result struct {
	Data    interface{}
	Code    string `json:"code"`
	Message string `json:"message"`
}

func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}

// GetMD5Hash MD5
func GetMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

// Base64Encode encode byte to string
func Base64Encode(src []byte) string {
	return string(base64.StdEncoding.EncodeToString(src))
}

// Base64Decode decode string
func Base64Decode(src string) ([]byte, error) {
	return base64.StdEncoding.DecodeString(src)
}

// GetUIDByToken get user id by token
func GetUIDByToken(token string, c *gin.Context) int {
	session := sessions.Default(c)
	tokenValue := session.Get(token)
	tokenValueString := tokenValue.(string)
	enbyte, err := Base64Decode(tokenValueString)
	CheckErr(err)
	i := strings.Index(string(enbyte), "_")
	id := string(enbyte)[:i]
	uid, err := strconv.Atoi(id)
	CheckErr(err)
	return uid
}

func res() {

}

/*
c.JSON(200, gin.H{
		"account":  user.Account,
		"password": user.Password,
		"id":       id,
		"dbspwd":   pwd,
	})
*/
